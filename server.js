var http = require('http');
var fs = require('fs');
var mysql = require("mysql");
var qs = require('querystring');
var pageHtml = fs.readFileSync('andromeda.html');
var express = require('express');

var con = mysql.createConnection({
  host: "localhost",
  user: "andromeda",
  password: "tracker",
  database: "locations"
});

con.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
});

var app = express()

app.get('/', function (req, res) {
		res.writeHead(200, {'Content-Type': 'text/html'});
		res.end(pageHtml);
})

app.get('/track', function (req, res) {
	var pointsArray = [];
	req.setEncoding('utf-8');
	console.log("uid: " + req.query.uid);
		con.query("SELECT * FROM points WHERE uid = ?", [req.query.uid], function(mysqlErr,mysqlRes){
			if(mysqlErr) throw mysqlErr;			
            for (var i=0; i<mysqlRes.length; i++) pointsArray[i] = mysqlRes[i].point;			
			res.writeHead(200, {'Content-Type': 'text/json'});
			res.end(JSON.stringify(pointsArray));
		});
});

app.post('/', function(req, res){
	req.setEncoding('utf-8');
	req.on('data', function(data) {	
		var postData = JSON.parse(data);
		con.query("INSERT INTO points (point,uid) VALUES (ST_GEOMFROMTEXT('POINT(? ?)'), ?)",[postData.point[0], postData.point[1], postData.uid], function(mysqlErr,mysqlRes){
			if(mysqlErr) throw mysqlErr;
			console.log('mysqlRes.insertId: ', mysqlRes.insertId);
			}); 
		});
		
		req.on('end', function() {
			res.writeHead(200, {'Content-Type': 'text/json'});
			res.end("true");
		});
})

app.listen(1337, function () {
  console.log('Example app listening on port 1337')
})